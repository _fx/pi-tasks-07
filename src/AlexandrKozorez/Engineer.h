#pragma once

#include "Employee.h"

class Engineer : public Employee, public WorkTime, public Project
{
	protected:
		int base;
		double part;
		string proj;
	public:
		Engineer() {}
		~Engineer() {}
		void setBase(int base) { this->base = base; }
		void setPart(double part) { this->part = part; }
		void setProj(string proj) { this->proj = proj; }
		int getBase() const { return base; }
		double getPart() const { return part; }
		string getProj() const { return proj; }
		double salary_type_1(int worktime, int base) override
		{
			return worktime*base; 
		}
		double salary_type_2(int budget, double part) override
		{
			return budget*part / per;
		}
		void calculate() override 
		{
			this->payment = (salary_type_1(this->worktime, this->base) + salary_type_2(amount, part))*ratio1; 
		}
};

class Programmer : public Engineer
{
	public:
		Programmer(int id = 0, string name = "", int base = 0, string proj = "", double part = 0, int worktime = 0, int payment = 0)
		{
			this->id = id;
			this->name = name;
			this->base = base;
			this->proj = proj;
			this->worktime = worktime;
			this->payment = payment;
			this->part = part;
		}
		~Programmer() {}
};

class Tester : public Engineer
{
	public:
		Tester(int id = 0, string name = "", int base = 0, string proj = "", double part = 0, int worktime = 0, int payment = 0)
		{
			this->id = id;
			this->name = name;
			this->base = base;
			this->proj = proj;
			this->worktime = worktime;
			this->payment = payment;
			this->part = part;
		}
		~Tester() {}
};

class TeamLeader : public Programmer, public Heading 
{
	protected:
		int qual;
	public:
		TeamLeader(int id = 0, string name = "", int base = 0, string proj = "", int qual = 0, double part = 0, int worktime = 0, int payment = 0)
		{
			this->id = id;
			this->name = name;
			this->base = base;
			this->proj = proj;
			this->qual = qual;
			this->worktime = worktime;
			this->payment = payment;
			this->part = part;
		}
		~TeamLeader() {}
		double salary_type_3(int qual) override
		{
			return qual*amount;
		}
		void calculate() override
		{
			this->payment = (salary_type_3(qual) + salary_type_1(worktime, base) + salary_type_2(amount, part))*ratio2;
		}
};
